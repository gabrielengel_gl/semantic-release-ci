ARG NODE_VERSION=20.7
ARG SEMANTIC_REL_VERSION=22.0.5
ARG SEMANTIC_REL_GL_VERSION=12.0.6
ARG COMMITLINT_VERSION=17.7.1

FROM node:$NODE_VERSION-buster-slim

# hadolint ignore=DL3008
RUN apt-get update && apt-get install -y --no-install-recommends git-core ca-certificates && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
RUN npm install -g semantic-release@"$SEMANTIC_REL_VERSION" @semantic-release/gitlab@"$SEMANTIC_REL_GL_VERSION" && \
    npm install -g commitlint@"$COMMITLINT_VERSION" @commitlint/config-conventional@"$COMMITLINT_VERSION"

COPY .releaserc.yml /usr/local/lib/
COPY commitlint.config.js /usr/local/lib/