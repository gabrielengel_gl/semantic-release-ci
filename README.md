# Semantic Release [![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://gitlab.com/gabrielengel_gl/semantic-release-ci)

This is a demo project implements [Semantic Release](https://semantic-release.gitbook.io/semantic-release/v/beta/recipes/recipes/gitlab-ci) GitLab CI template.
With it you can automatically determine the version bump of [Semantic Versioning](https://semver.org) by using the conventions of [Conventional Commits (v1.0.0)](https://www.conventionalcommits.org/en/v1.0.0/).

- Lints your commits if they follow the convention of [Conventional Commits (v1.0.0)](https://www.conventionalcommits.org/en/v1.0.0/)
- Commit messages starting with `fix:` trigger a patch version bump
- Commit messages starting with `feat:` trigger a minor version bump
- Commit messages containing `BREAKING CHANGE:` trigger a major version bump

## CI/CD

### Branch & MR Pipelines

- `lint:` Commitlint to check if all commits follow the convention of [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
- `release:` Tests the release creation for GitLab

### Pipeline on default branch

- `lint:` Commitlint to check if all commits follow the convention of [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
- `release:` Semantic Release is analyzing the upcoming version bump and creating a new release for version changes.

## How to use it

We have implemented the template to allow you use Semantic Release with only icluding the Job and setting a CI/CD variable.

The template also builds a custom docker image that has the configuration files injected and allows usage of this template without adding any configuration files to the repository. The job `before_script` allows users to still use a custom `.releaserc.yml` file.

```yaml
- |
    if [ ! -f .releaserc.yml ]; then
    mv /usr/local/lib/.releaserc.yml .
    fi
```

### Include the CI/CD template

To configure this template in your project all you have to do is include:

```yaml
include:
  - project: "gabrielengel_gl/semantic-release-ci"
    file:
      - "Jobs/SemanticRelease.gitlab-ci.yml"
```

### Add Access Token as CI/CD variable

The template requires `$SEMVER_TOKEN` as env variable with api access to your repository.

- Generate [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with api access to your repository. You can use either from a technical or personal user.
- Set `$SEMVER_TOKEN` [CI/CD Variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) as `masked` and `protected`.

## Optional Repository Configuration

### Adding Push Rules

While we have added a commitlint to check if the commit messages are following convention of [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) you can also check this via [Push Rules](https://docs.gitlab.com/ee/user/project/repository/push_rules.html#override-global-push-rules-per-project). This has the advantage that you will get direct feedback when using `git push` and avoid unnecessary pipelines.

The following push rules allow you to reject commits not following the conventional commits. You can adapt addiotional key-words with your preference.

```bash
^((Notes added by \'git notes add\'))|((((feat|fix|docs|style|refactor|perf|test|chore|bug|breaking)(\(.+\))?:)|Merge|Resolve|Revert))\s.{1,1000}
```

To disambe commitlint e.g. when using Push Rules, you can add:

```yaml
variables:
    COMMITLINT_DISABLED: "true"
```

### Merge Request Configuration

We suggest to enable [fast-forward merges](https://docs.gitlab.com/ee/user/project/merge_requests/methods/#fast-forward-merge) in your repository, so GitLab doesn't add a MR-commit.

### Add Git Commit Guidelines

We would also advise to add commit guidelines to the project either referencing this project or copy & paste the following Git Commit Guidelines.

```md
## Git Commit Guidelines

This project uses [Semantic Versioning](https://semver.org). We use commit
messages to automatically determine the version bumps, so they should adhere to
the conventions of [Conventional Commits (v1.0.0)](https://www.conventionalcommits.org/en/v1.0.0/).

## Automatic versioning

Each push to your default branch triggers a [`semantic-release`](https://semantic-release.gitbook.io/semantic-release/)
CI job that determines and pushes a new version tag (if any) based on the
last version tagged and the new commits pushed. Notice that this means that if a
Merge Request contains, for example, several `feat:` commits, only one minor
version bump will occur on merge. If your Merge Request includes several commits
you may prefer to ignore the prefix on each individual commit and instead add
an empty commit sumarizing your changes like so:

`git commit --allow-empty -m '[feat|fix]: <changelog summary message> [BREAKING CHANGE]: <breaking change description>'`

### TL;DR

- Lints your commits if they follow the convention of [Conventional Commits (v1.0.0)](https://www.conventionalcommits.org/en/v1.0.0/)
- Commit messages starting with `fix:` trigger a patch version bump
- Commit messages starting with `feat:` trigger a minor version bump
- Commit messages containing `BREAKING CHANGE:` trigger a major version bump
```

### Adding Badge to Semantic Release documentation

You can also add the badge [![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://gitlab.com/gabrielengel_gl/semantic-release-ci) for more info.
